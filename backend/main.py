import os
from flask import Flask
from pages.background import background
from pages.portrait import portrait
from flask_cors import CORS

app = Flask(__name__)
CORS(app)

app.config['UPLOAD_PATH'] = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'static')

app.register_blueprint(background, url_prefix="/background")
app.register_blueprint(portrait, url_prefix="/portrait")

@app.route("/")
def home():
	return "It image manipulation Backend for learning by <b>Reza Pahlevi Yahya</b>"

if __name__ == "__main__":
  app.run(debug=True, host='0.0.0.0')