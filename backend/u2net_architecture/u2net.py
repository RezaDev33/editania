import os
from skimage import io
import torch
from torch.autograd import Variable
from torch.utils.data import DataLoader
from torchvision import transforms

import numpy as np
from PIL import Image
import glob

from u2net_architecture.data_loader import RescaleT
from u2net_architecture.data_loader import ToTensorLab
from u2net_architecture.data_loader import SalObjDataset

from u2net_architecture.model import U2NET # full size version 173.6 MB
from u2net_architecture.model import U2NETP # small version u2net 4.7 MB

class U2NET_SILIENT_OBJECT():
	def __init__(self, version_name = 'u2net', ori_images = []):
		self.img_name_list = ori_images
		self.model_dir = os.path.join(os.getcwd(), 'u2net_architecture/saved_models', version_name, version_name + '.pth')
		self.model = U2NET(3,1) if version_name == 'u2net' else U2NETP(3,1)
		self.test_salobj_dataset = SalObjDataset(img_name_list = ori_images,lbl_name_list = [], transform=transforms.Compose([RescaleT(320), ToTensorLab(flag=0)]))
		self.test_salobj_dataloader = DataLoader(self.test_salobj_dataset,batch_size=1,shuffle=False,num_workers=1) 

		if torch.cuda.is_available():
			self.model.load_state_dict(torch.load(self.model_dir))
			self.model.cuda()
		else:
			self.model.load_state_dict(torch.load(self.model_dir, map_location='cpu'))
		self.model.eval()

	def normPRED(self,d):
			ma = torch.max(d)
			mi = torch.min(d)

			dn = (d-mi)/(ma-mi)

			return dn

	def save_output(self,image_name,pred):

			predict = pred
			predict = predict.squeeze()
			predict_np = predict.cpu().data.numpy()

			im = Image.fromarray(predict_np*255).convert('RGB')
			image = io.imread(image_name)
			imo = im.resize((image.shape[1],image.shape[0]),resample=Image.BILINEAR)

			return imo

	def save_output_crop(self,image_name,pred):

			predict = pred
			predict = predict.squeeze()
			predict_np = predict.cpu().data.numpy()

			im = Image.fromarray(predict_np*255).convert('L')
			image = Image.fromarray(io.imread(image_name))
			imo = im.resize(image.size,resample=Image.BILINEAR)
			empty = Image.new("RGBA", image.size)
			imageo = Image.composite(image, empty, imo)

			return imageo

	def process(self):
		image = []
		for i_test, data_test in enumerate(self.test_salobj_dataloader):

				print("inferencing:",self.img_name_list[i_test].split(os.sep)[-1])

				inputs_test = data_test['image']
				inputs_test = inputs_test.type(torch.FloatTensor)

				if torch.cuda.is_available():
						inputs_test = Variable(inputs_test.cuda())
				else:
						inputs_test = Variable(inputs_test)

				d1,d2,d3,d4,d5,d6,d7= self.model(inputs_test)

				# normalization
				pred = d1[:,0,:,:]
				pred = self.normPRED(pred)

				# ouput image result
				image.append(self.save_output_crop(self.img_name_list[i_test],pred))

				del d1,d2,d3,d4,d5,d6,d7

		return image


class U2NET_HUMAN_SEGMENTATION():
	def __init__(self, version_name = 'u2net', ori_images = []):
		self.img_name_list = ori_images
		self.model_dir = os.path.join(os.getcwd(), 'u2net_architecture/saved_models', version_name + '_human_seg', version_name + '_human_seg.pth')
		self.model = U2NET(3,1) if version_name == 'u2net' else U2NETP(3,1)
		self.test_salobj_dataset = SalObjDataset(img_name_list = ori_images,lbl_name_list = [], transform=transforms.Compose([RescaleT(320), ToTensorLab(flag=0)]))
		self.test_salobj_dataloader = DataLoader(self.test_salobj_dataset,batch_size=1,shuffle=False,num_workers=1) 

		if torch.cuda.is_available():
			self.model.load_state_dict(torch.load(self.model_dir))
			self.model.cuda()
		else:
			self.model.load_state_dict(torch.load(self.model_dir, map_location='cpu'))
		self.model.eval()

	def normPRED(self,d):
			ma = torch.max(d)
			mi = torch.min(d)

			dn = (d-mi)/(ma-mi)

			return dn

	def save_output(self,image_name,pred):

			predict = pred
			predict = predict.squeeze()
			predict_np = predict.cpu().data.numpy()

			im = Image.fromarray(predict_np*255).convert('RGB')
			image = io.imread(image_name)
			imo = im.resize((image.shape[1],image.shape[0]),resample=Image.BILINEAR)

			return imo

	def save_output_crop(self,image_name,pred):

			predict = pred
			predict = predict.squeeze()
			predict_np = predict.cpu().data.numpy()

			im = Image.fromarray(predict_np*255).convert('L')
			image = Image.fromarray(io.imread(image_name))
			imo = im.resize(image.size,resample=Image.BILINEAR)
			empty = Image.new("RGBA", image.size)
			imageo = Image.composite(image, empty, imo)

			return imageo

	def process(self):
		image = []
		for i_test, data_test in enumerate(self.test_salobj_dataloader):

				print("inferencing:",self.img_name_list[i_test].split(os.sep)[-1])

				inputs_test = data_test['image']
				inputs_test = inputs_test.type(torch.FloatTensor)

				if torch.cuda.is_available():
						inputs_test = Variable(inputs_test.cuda())
				else:
						inputs_test = Variable(inputs_test)

				d1,d2,d3,d4,d5,d6,d7= self.model(inputs_test)

				# normalization
				pred = d1[:,0,:,:]
				pred = self.normPRED(pred)

				# ouput image result
				image.append(self.save_output_crop(self.img_name_list[i_test],pred))

				del d1,d2,d3,d4,d5,d6,d7

		return image


class U2NET_PORTRAIT_DRAWING():
	def __init__(self, version_name = 'u2net_portrait', ori_images = []):
		self.img_name_list = ori_images
		self.model_dir = os.path.join(os.getcwd(), 'u2net_architecture/saved_models', version_name, version_name + '.pth')
		self.model = U2NET(3,1)
		self.test_salobj_dataset = SalObjDataset(img_name_list = ori_images,lbl_name_list = [], transform=transforms.Compose([RescaleT(320), ToTensorLab(flag=0)]))
		self.test_salobj_dataloader = DataLoader(self.test_salobj_dataset,batch_size=1,shuffle=False,num_workers=1) 

		if torch.cuda.is_available():
			self.model.load_state_dict(torch.load(self.model_dir))
			self.model.cuda()
		else:
			self.model.load_state_dict(torch.load(self.model_dir, map_location='cpu'))
		self.model.eval()

	def normPRED(self,d):
			ma = torch.max(d)
			mi = torch.min(d)

			dn = (d-mi)/(ma-mi)

			return dn

	def save_output(self,image_name,pred):

			predict = pred
			predict = predict.squeeze()
			predict_np = predict.cpu().data.numpy()

			im = Image.fromarray(predict_np*255).convert('RGB')
			image = io.imread(image_name)
			imo = im.resize((image.shape[1],image.shape[0]),resample=Image.BILINEAR)

			return imo

	def process(self):
		image = []
		for i_test, data_test in enumerate(self.test_salobj_dataloader):

				print("inferencing:",self.img_name_list[i_test].split(os.sep)[-1])

				inputs_test = data_test['image']
				inputs_test = inputs_test.type(torch.FloatTensor)

				if torch.cuda.is_available():
						inputs_test = Variable(inputs_test.cuda())
				else:
						inputs_test = Variable(inputs_test)

				d1,d2,d3,d4,d5,d6,d7= self.model(inputs_test)

				# normalization
				pred = d1[:,0,:,:]
				pred = self.normPRED(pred)

				# ouput image result
				image.append(self.save_output(self.img_name_list[i_test],pred))

				del d1,d2,d3,d4,d5,d6,d7

		return image