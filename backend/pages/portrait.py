import os
from flask import Blueprint, request, redirect, current_app as app
from werkzeug.utils import secure_filename
from u2net_architecture.u2net import U2NET_PORTRAIT_DRAWING

portrait = Blueprint('portrait', __name__, static_folder="static", static_url_path="/static")

def allowed_file(filename, extension):
	return '.' in filename and \
					filename.rsplit('.', 1)[1].lower() in extension

@portrait.route('drawing/<model>', methods=['POST'])
def portraitDrawing(model):
	allowed_extension = {'png', 'jpg', 'jpeg'}
	uploaded_files = request.files.getlist("images[]")
	
	origin_files = []
	origin_files_name = []

	for file in uploaded_files:
		if file.filename == '':
			return redirect(request.url)
		if file and allowed_file(file.filename, allowed_extension):
			filename = secure_filename(file.filename)
			filepath = os.path.join(app.config['UPLOAD_PATH'], "before", filename)
			file.save(filepath)
			origin_files.append(filepath)
			origin_files_name.append(filename)

	if model == "u2net_portrait":
		u2net = U2NET_PORTRAIT_DRAWING(model,origin_files)
		result_files = u2net.process()
		for i_file, file in enumerate(result_files):
			origin_files_name[i_file] = origin_files_name[i_file].split(".")[0]+".png"
			filepath = os.path.join(app.config['UPLOAD_PATH'], "after", origin_files_name[i_file])
			file.save(filepath)
	
	return "Processing Complite"

