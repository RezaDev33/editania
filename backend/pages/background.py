import os
from flask import Blueprint, json, request, redirect, current_app as app, jsonify, send_from_directory
from werkzeug.utils import secure_filename
from u2net_architecture.u2net import U2NET_SILIENT_OBJECT, U2NET_HUMAN_SEGMENTATION

background = Blueprint('background', __name__, static_folder="static", static_url_path="/static")

def allowed_file(filename, extension):
	return '.' in filename and \
					filename.rsplit('.', 1)[1].lower() in extension

@background.route('remove/<model>', methods=['POST'])
def removeBackground(model):
	uploaded_files = request.files.getlist("images[]")
	allowed_extension = {'png', 'jpg', 'jpeg'}
	human_version = request.form.get('human_version')

	origin_files = []
	origin_files_name = []

	for file in uploaded_files:
		if file.filename == '':
			return redirect(request.url)
		if file and allowed_file(file.filename, allowed_extension):
			filename = secure_filename(file.filename)
			filepath = os.path.join(app.config['UPLOAD_PATH'], "before", filename)
			file.save(filepath)
			origin_files.append(filepath)
			origin_files_name.append(filename)

	if model == "u2net" or model == "u2netp":
		if human_version == "1":
			u2net = U2NET_HUMAN_SEGMENTATION(model,origin_files)
		else:
			u2net = U2NET_SILIENT_OBJECT(model,origin_files)
		result_files = u2net.process()
		for i_file, file in enumerate(result_files):
			origin_files_name[i_file] = origin_files_name[i_file].split(".")[0]+".png"
			filepath = os.path.join(app.config['UPLOAD_PATH'], "after", origin_files_name[i_file])
			file.save(filepath)
	
	return jsonify({
		"status": 200,
		"message": "success",
		"filename": json.dumps(origin_files_name)
	})

@background.route('/download/<path:filename>')
def download(filename):
  after = os.path.join(app.config['UPLOAD_PATH'], "after")
  return send_from_directory(after, filename)