## Editania
 
Editania adalah aplikasi pengolahan citra digital berbasis website. Pada aplikasi dapat melakukan penghapusan background pada gambar mengunakan object segmentation dengan arsitektur [U-2-Net](https://github.com/xuebinqin/U-2-Net).

### Tentang Aplikasi
- Dibuat dengan mengunakan Flask untuk Backend dan Nuxt.js untuk Frontend.
- Dapat menghapus backgroud pada foto dan video dengan mengunakan arsitektur [U-2-Net](https://github.com/xuebinqin/U-2-Net).
- Dapat mengambar sketsa wajah pada foto dengan mengunakan arsitektur [U-2-Net](https://github.com/xuebinqin/U-2-Net).

### App Preview
![Home](/asset/editania_home.png "Home")
![Feature](/asset/editania_feature.png "Feature")
